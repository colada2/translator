Internationalization
====================

The message translation service translates a text message from one language to another.


##Structure of function `translate()`
string translate ( string $category, string $message [, array $params = [] ] [, string $locale = '' ] [, int $fallbackLevel = Translate::FALLBACK_ALL ] )

Parameters:

* category  - category of message
* message - key of message
* params - list of parameters for replacement placeholders
* locale - use it if you want to get translation from another language (not default)
* fallbackLevel - please, read topic about FALLBACK MODE below


###Simple usage:

* Wrap every text message that needs to be translated in a call to the `translate()` function;

The method `translate()` can be used like the following,

```php
echo translate('app', 'This is a string to translate!');
```

where the second parameter refers to the text message to be translated, while the first parameter refers to
the name of the category which is used to categorize the message.


### Message Formatting
When translating a message, you can embed some placeholders and have them replaced by dynamic parameter values.
You can even use special placeholder syntax to have the parameter values formatted according to the target language.
In this subsection, we will describe different ways of formatting messages.


### Message Parameters

In a message to be translated, you can embed one or multiple parameters (also called placeholders) so that they can be
replaced by the given values. By giving different sets of values, you can variate the translated message dynamically.
In the following example, the placeholder `{username}` in the message `'Hello, {username}!'` will be replaced
by `'Alexander'` and `'Qiang'`, respectively.

```php
$username = 'Alexander';
// display a translated message with username being "Alexander"
echo translate('app', 'Hello, {username}!', [
    'username' => $username,
]);

$username = 'Qiang';
// display a translated message with username being "Qiang"
echo translate('app', 'Hello, {username}!', [
    'username' => $username,
]);
```

While translating a message containing placeholders, you should leave the placeholders as is. This is because the placeholders
will be replaced with the actual values when you call `translate()` to translate a message.

You can use either *named placeholders* or *positional placeholders*, but not both, in a single message.

The previous example shows how you can use named placeholders. That is, each placeholder is written in the format of
`{name}`, and you provide an associative array whose keys are the placeholder names
(without the curly brackets) and whose values are the corresponding values placeholder to be replaced with.

Positional placeholders use zero-based integer sequence as names which are replaced by the provided values
according to their positions in the call of `translate()`. In the following example, the positional placeholders
`{0}`, `{1}` and `{2}` will be replaced by the values of `$price`, `$count` and `$subtotal`, respectively.

```php
$price = 100;
$count = 2;
$subtotal = 200;
echo translate('app', 'Price: {0}, Count: {1}, Subtotal: {2}', [$price, $count, $subtotal]);
```

> Tip: In most cases you should use named placeholders. This is because the names will make the translators
> understand better the whole messages being translated.


### Parameter Formatting

You can specify additional formatting rules in the placeholders of a message so that the parameter values can be
formatted properly before they replace the placeholders. In the following example, the price parameter value will be
treated as a number and formatted as a currency value:

```php
$price = 100;
echo translate('app', 'Price: {0,number,currency}', $price);
```

> Note: Parameter formatting requires the installation of the [intl PHP extension](http://www.php.net/manual/en/intro.intl.php).

You can use either the short form or the full form to specify a placeholder with formatting:

```
short form: {name,type}
full form: {name,type,style}
```

> Note: If you need to use special characters such as `{`, `}`, `'`, `#`, wrap them in `'`:
>
```php
echo translate('app', "Example of string with ''-escaped characters'': '{' '}' '{test}' {count,plural,other{''count'' value is # '#{}'}}", ['count' => 3]);
```

Complete format is described in the [ICU documentation](http://icu-project.org/apiref/icu4c/classMessageFormat.html).
In the following we will show some common usages.

#### Number

The parameter value is treated as a number. For example,

```php
$sum = 42;
echo translate('app', 'Balance: {0,number}', $sum);
```

You can specify an optional parameter style as `integer`, `currency`, or `percent`:

```php
$sum = 42;
echo translate('app', 'Balance: {0,number,currency}', $sum);
```

You can also specify a custom pattern to format the number. For example,

```php
$sum = 42;
echo translate('app', 'Balance: {0,number,,000,000000}', $sum);
```

Characters used in the custom format could be found in
[ICU API reference](http://icu-project.org/apiref/icu4c/classicu_1_1DecimalFormat.html) under "Special Pattern Characters"
section.


The value is always formatted according to the locale you are translating to i.e. you cannot change decimal or thousands
separators, currency symbol etc. without changing translation locale.

#### Date

The parameter value should be formatted as a date. For example,

```php
echo translate('app', 'Today is {0,date}', [time()]);
```

You can specify an optional parameter style as `short`, `medium`, `long`, or `full`:

```php
echo translate('app', 'Today is {0,date,short}', [time()]);
```

You can also specify a custom pattern to format the date value:

```php
echo translate('app', 'Today is {0,date,yyyy-MM-dd}', time());
```

[Formatting reference](http://icu-project.org/apiref/icu4c/classicu_1_1SimpleDateFormat.html).


#### Time

The parameter value should be formatted as a time. For example,

```php
echo translate('app', 'It is {0,time}', [time()]);
```

You can specify an optional parameter style as `short`, `medium`, `long`, or `full`:

```php
echo translate('app', 'It is {0,time,short}', [time()]);
```

You can also specify a custom pattern to format the time value:

```php
echo translate('app', 'It is {0,date,HH:mm}', [time()]);
```

[Formatting reference](http://icu-project.org/apiref/icu4c/classicu_1_1SimpleDateFormat.html).


#### Spellout

The parameter value should be treated as a number and formatted as a spellout. For example,

```php
// may produce "42 is spelled as forty-two"
echo translate('app', '{n,number} is spelled as {n,spellout}', ['n' => 42]);
```

By default the number is spelled out as cardinal. It could be changed:

```php
// may produce "I am forty-seventh agent"
echo translate('app', 'I am {n,spellout,%spellout-ordinal} agent', ['n' => 47]);
```

Note that there should be no space after `spellout,` and before `%`.

To get a list of options available for locale you're using check
"Numbering schemas, Spellout" at [http://intl.rmcreative.ru/](http://intl.rmcreative.ru/).

#### Ordinal

The parameter value should be treated as a number and formatted as an ordinal name. For example,

```php
// may produce "You are the 42nd visitor here!"
echo translate('app', 'You are the {n,ordinal} visitor here!', ['n' => 42]);
```

Ordinal supports more ways of formatting for languages such as Spanish:

```php
// may produce 471ª
echo translate('app', '{n,ordinal,%digits-ordinal-feminine}', ['n' => 471]);
```

Note that there should be no space after `ordinal,` and before `%`.

To get a list of options available for locale you're using check
"Numbering schemas, Ordinal" at [http://intl.rmcreative.ru/](http://intl.rmcreative.ru/).

#### Duration

The parameter value should be treated as the number of seconds and formatted as a time duration string. For example,

```php
// may produce "You are here for 47 sec. already!"
echo translate('app', 'You are here for {n,duration} already!', ['n' => 47]);
```

Duration supports more ways of formatting:

```php
// may produce 130:53:47
echo translate('app', '{n,duration,%in-numerals}', ['n' => 471227]);
```

Note that there should be no space after `duration,` and before `%`.

To get a list of options available for locale you're using check
"Numbering schemas, Duration" at [http://intl.rmcreative.ru/](http://intl.rmcreative.ru/).

#### Plural

Different languages have different ways to inflect plurals. I provide a convenient way for translating messages in
different plural forms that works well even for very complex rules. Instead of dealing with the inflection rules directly,
it is sufficient to provide the translation of inflected words in certain situations only. For example,

```php
// When $n = 0, it may produce "There are no cats!"
// When $n = 1, it may produce "There is one cat!"
// When $n = 42, it may produce "There are 42 cats!"
echo translate('app', 'There {n,plural,=0{are no cats} =1{is one cat} other{are # cats}}!', ['n' => $n]);
```

In the plural rule arguments above, `=` means explicit value. So `=0` means exactly zero, `=1` means exactly one.
`other` stands for any other value. `#` is replaced with the value of `n` formatted according to target language.

Plural forms can be very complicated in some languages. In the following Russian example, `=1` matches exactly `n = 1`
while `one` matches `21` or `101`:

```
Здесь {n,plural,=0{котов нет} =1{есть один кот} one{# кот} few{# кота} many{# котов} other{# кота}}!
```

These `other`, `few`, `many` and other special argument names vary depending on language. To learn which ones you should
specify for a particular locale, please refer to "Plural Rules, Cardinal" at [http://intl.rmcreative.ru/](http://intl.rmcreative.ru/).
Alternatively you can refer to [rules reference at unicode.org](http://unicode.org/repos/cldr-tmp/trunk/diff/supplemental/language_plural_rules.html).

> Note: The above example Russian message is mainly used as a translated message, not an original message, unless you set
> the [[$sourceLanguage|source language]] of your application as `ru-RU` and translating from Russian.
>
> When a translation is not found for an original message specified in `translate()` call, the plural rules for the
> [[$sourceLanguage|source language]] will be applied to the original message.

There's an `offset` parameter for the cases when the string is like the following:

```php
$likeCount = 2;
echo translate('app', 'You {likeCount,plural,
    offset: 1
    =0{did not like this}
    =1{liked this}
    one{and one other person liked this}
    other{and # others liked this}
}', [
    'likeCount' => $likeCount
]);

// You and one other person liked this
```

#### Ordinal selection

The parameter type of `selectordinal` is meant to choose a string based on language rules for ordinals for the
locale you are translating to:

```php
$n = 3;
echo translate('app', 'You are the {n,selectordinal,one{#st} two{#nd} few{#rd} other{#th}} visitor', ['n' => $n]);
// For English it outputs:
// You are the 3rd visitor

// Translation
'You are the {n,selectordinal,one{#st} two{#nd} few{#rd} other{#th}} visitor' => 'Вы {n,selectordinal,other{#-й}} посетитель',

// For Russian translation it outputs:
// Вы 3-й посетитель
```

The format is very close to what's used for plurals. To learn which arguments you should specify for a particular locale,
please refer to "Plural Rules, Ordinal" at [http://intl.rmcreative.ru/](http://intl.rmcreative.ru/).
Alternatively you can refer to [rules reference at unicode.org](http://unicode.org/repos/cldr-tmp/trunk/diff/supplemental/language_plural_rules.html).


### Handling missing translations
We can pass 4th parameter to `translate function`. It is fallback mode. If there is not translation for current language:

* **Translator::FALLBACK_ALL** - _By default._ Firstly we try to find it in default language (Translator::FALLBACK_LOCALE) and then we use key as translation(Translator::FALLBACK_KEY).
* Translator::FALLBACK_NONE - We will get `null`
* Translator::FALLBACK_KEY - We use key as translation.
* Translator::FALLBACK_LOCALE - We try to find it in default language.